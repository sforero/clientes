from django.apps import AppConfig


class ConcesionariosConfig(AppConfig):
    name = 'concesionarios'
