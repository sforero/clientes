from django.conf.urls import url
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from usuarios.views import (
    clienteList,
    clienteCreate,
    clienteUpdate,
    vehiculoList,
    vehiculoCreate,
    vehiculoUpdate,
    vehiculoDelete,
    clientevehiculoCreate,
    clientevehiculoList
)
from django.contrib.auth.models import User

urlpatterns = [

    url(
        r'^login/$',
        LoginView.as_view(
            template_name='usuarios/login.html'
        ),
        name='login'
    ),
    url(
        r'^logout/$',
        LogoutView.as_view(next_page=reverse_lazy('login')),
        name='logout'
    ),
    url(
        r'clientes/$',
        clienteList.as_view(),
        name='index'
    ),
    url(
        r'crear-cliente/$',
        clienteCreate.as_view(),
        name='Createcliente'
    ),
    url(
        r'^Update-cliente/(?P<pk>\d+)$',
        clienteUpdate.as_view(),
        name='Updatecliente'
    ),
    url(
        r'vehiculos/$',
        vehiculoList.as_view(),
        name='indexVehiculo'
    ),
    url(
        r'lista-total/$',
        clientevehiculoList.as_view(),
        name='indexclienteVehiculo'
    ),
    url(
        r'crear-vehiculo/$',
        vehiculoCreate.as_view(),
        name='Createvehiculo'
    ),
    url(
        r'crear-clientevehiculo/$',
        clientevehiculoCreate.as_view(),
        name='Createclientevehiculo'
    ),
    url(
        r'^update-vehiculo/(?P<pk>\d+)$',
        vehiculoUpdate.as_view(),
        name='Updatevehiculo'
    ),
    url(
       r'^delete-vehiculo/(?P<pk>\d+)$',
       vehiculoDelete.as_view(),
       name='Deletevehiculo'
       ),
    ]
