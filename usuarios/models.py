from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class Vehiculo(models.Model):
    marca = models.CharField(max_length=30)
    modelo = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return (self.marca + ' ' + self.modelo)

    def save(self, *args, **kwargs):
        self.nombre_completo = (
          self.marca + ' ' + self.modelo
        )
        super(Vehiculo, self).save(*args, **kwargs)


def full_name(self):
        return(self.marca + '' + self.modelo)


def validate_cedula(value):
    if not value.isdigit():
        raise validationError(
            'No Cumple'
        )


class Cliente(models.Model):
    nombrecliente = models.CharField(max_length=30)
    apellidoscliente = models.CharField(max_length=30, blank=True)
    documento = models.CharField(max_length=30, validators=[validate_cedula])

    def __str__(self):
        return (self.nombrecliente + ' ' + self.apellidoscliente)

    def save(self, *args, **kwargs):
            self.nombre_completo = (
              self.nombrecliente + ' ' + self.apellidoscliente
            )
            super(Cliente, self).save(*args, **kwargs)

    def full_name(self):
            return(self.nombrecliente + '' + self.apellidoscliente)


class Clientevehiculo(models.Model):
    vehiculo = models.ForeignKey(
     Vehiculo, on_delete=models.CASCADE,
    )
    cliente = models.ForeignKey(
     Cliente, on_delete=models.CASCADE,
    )
    matricula = models.CharField(max_length=30)
    kilometros = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return (self.matricula + ' ' + self.kilometros)

    def full_name(self):
        return(self.matricula + '' + self.kilometros)
