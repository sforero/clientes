from django.shortcuts import render
from django.http import HttpResponse
from usuarios.models import Cliente
from django.views.generic import ListView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from usuarios.models import Cliente, Vehiculo, Clientevehiculo


class clienteList(LoginRequiredMixin, ListView):
    model = Cliente
    context_object_name = 'clientes'
    template_name = 'usuarios/index.html'


class vehiculoList(LoginRequiredMixin, ListView):
    model = Vehiculo
    context_object_name = 'clientes'
    template_name = 'usuarios/indexVehiculo.html'


class clientevehiculoList(LoginRequiredMixin, ListView):
    model = Clientevehiculo
    context_object_name = 'clientes'
    template_name = 'usuarios/indexclienteVehiculo.html'


class clienteCreate(CreateView):
    model = Cliente
    fields = ['nombrecliente', 'apellidoscliente', 'documento']
    template_name = 'usuarios/createcliente.html'
    success_url = reverse_lazy('index')


class vehiculoCreate(CreateView):
    model = Vehiculo
    fields = ['marca', 'modelo']
    template_name = 'usuarios/createvehiculo.html'
    success_url = reverse_lazy('indexVehiculo')


class clientevehiculoCreate(CreateView):
    model = Clientevehiculo
    fields = ['matricula', 'kilometros', 'vehiculo', 'cliente']
    template_name = 'usuarios/createclientevehiculo.html'
    success_url = reverse_lazy('indexVehiculo')


class clienteUpdate(UpdateView):
    model = Cliente
    fields = ['nombrecliente', 'apellidoscliente', 'documento']
    template_name = 'usuarios/updatecliente.html'
    success_url = reverse_lazy('index')


class vehiculoUpdate(UpdateView):
    model = Vehiculo
    fields = ['marca', 'modelo']
    template_name = 'usuarios/updatevehiculo.html'
    success_url = reverse_lazy('indexVehiculo')


class vehiculoDelete(DeleteView):
    model = Vehiculo
    success_url = reverse_lazy('indexVehiculo')
