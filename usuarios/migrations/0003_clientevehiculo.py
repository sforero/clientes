# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-21 20:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0002_vehiculo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clientevehiculo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matricula', models.CharField(max_length=30)),
                ('kilometros', models.CharField(blank=True, max_length=30)),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='usuarios.Cliente')),
                ('vehiculo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='usuarios.Vehiculo')),
            ],
        ),
    ]
